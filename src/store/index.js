import Vuex from 'vuex';
import Vue from 'vue';
import companyData from './modules/company-data';

Vue.use(Vuex);
export default new Vuex.Store({
  modules: {
    companyData
  }
});
