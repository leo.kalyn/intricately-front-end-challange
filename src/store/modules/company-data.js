const state = {
  companyData: {
    name: '',
    spend: '',
    spendAbility: {
      min: '',
      max: '',
    },
    notes: ''
  },
  error: {
    name: '',
    spend: '',
    spendAbility: '',
    notes: ''
  }
};

const getters = {
  companyData: state => state.companyData,
  companyDataErrors: state => state.error,
};

const actions = {
  updateCompanyField({commit}, fieldData) {
    commit('updateField', fieldData)
  },
  validateField({commit}, fieldName) {
    const {companyData} = state;
    switch(fieldName) {
      case 'name':
        if(companyData.name === '') {
          commit('setError', {fieldName, error: 'Name must be present.'});
          return;
        }
        commit('setError', {fieldName, error: ''});
      break;
      case 'spendAbility':
        let error = '';
        if(companyData.spendAbility.min === '' && companyData.spendAbility.max === '') {
          commit('setError', {fieldName, error: ''});
          return;
        }
        if(companyData.spendAbility.min === ''){
          commit('setError', {fieldName, error: 'Min is required.'});
          return;
        }
        if(companyData.spendAbility.max === ''){
          commit('setError', {fieldName, error: 'Max is required.'});
          return;
        }
        if(companyData.spendAbility.min > companyData.spendAbility.max){
          console.log("max min")
          commit('setError', {fieldName, error: 'Min must be lesser or equal to Max.'});
          return;
        }
        commit('setError', {fieldName, error: ''});
      break;
    }
  },
};

const mutations = {
  updateField: (state, {fieldName, fieldValue}) => (state.companyData[fieldName] = fieldValue),
  setError: (state, {fieldName, error}) => (state.error[fieldName] = error),
};

export default {
  state,
  getters,
  actions,
  mutations,
}
