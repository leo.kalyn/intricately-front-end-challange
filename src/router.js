import Vue from 'vue'
import Router from 'vue-router'
import CompanyData from 'Pages/CompanyData.vue'
import CompanyPage from 'Pages/CompanyPage.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'company-data',
      component: CompanyData
    },
    {
      path: '/company-page',
      name: 'company-page',
      component: CompanyPage
    }
  ]
})
