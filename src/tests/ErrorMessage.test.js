import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex'

import ErrorMessage from '@/components/ErrorMessage.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('Input.vue', () => {
  const propsData = { fieldName: 'fieldName'};

  it('Renders ErrorMessage when an error is returned', () => {
    const store = new Vuex.Store({
      getters: {
        companyDataErrors: () => ({[propsData.fieldName]: 'Error'}),
      }
    });

    const wrapper = shallowMount(ErrorMessage, { store, localVue, propsData});

    expect(wrapper.find('.error__message').isVisible()).toBe(true);
  })

  it('does not renders ErrorMessage when an error is not returned', () => {
    const store = new Vuex.Store({
      getters: {
        companyDataErrors: () => ({[propsData.fieldName]: ''}),
      },
    });

    const wrapper = shallowMount(ErrorMessage, { store, localVue, propsData});
    expect(wrapper.find('.error__message').isVisible()).toBe(false);
  })
});
