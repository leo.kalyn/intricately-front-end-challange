import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex'

import Input from '@/components/Input.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('Input.vue', () => {
  const propsData = { fieldName: 'fieldName'};
  let actions, getters, store;

  beforeEach(() => {
    actions = {
      updateCompanyField: jest.fn(),
      validateField: jest.fn()
    };

    getters = {
      companyData: () => '',
      companyDataErrors: () =>  ({[propsData.fieldName]: 'Error'}),
    };

    store = new Vuex.Store({
      actions,
      getters,
    });
  })

  it('dispatches "updateCompanyField" when @input is triggered', () => {
    const wrapper = shallowMount(Input, { store, localVue, propsData})
    const input = wrapper.find('input')
    input.trigger('input')
    expect(actions.updateCompanyField).toHaveBeenCalled()
  })

  it('dispatches "validateField" when @blur is triggered', () => {
    const wrapper = shallowMount(Input, { store, localVue, propsData})
    const input = wrapper.find('input')
    input.trigger('blur');
    expect(actions.validateField).toHaveBeenCalled();
  })
});
